##Helping People to Pass their Test!

Here at the Graduate School of Motoring we have been providing first class driving tuition in the Limerick, Clare and Tipperary areas for almost twenty years.  

Offering lessons in car, car and trailer, truck and ambulance driver training as well as driving instructor training courses, all our instructors are RSA approved and ADI registered, and we have an unrivalled pass rate in all categories.

We take great pride in our high pass rate and customer satisfaction is always our priority. As well as this, we are able to offer the most competitive rates available.

Lessons can be arranged at times that suit you, and if required a pick up at your location can be accommodated.
